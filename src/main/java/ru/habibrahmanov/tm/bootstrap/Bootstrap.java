package ru.habibrahmanov.tm.bootstrap;

import ru.habibrahmanov.tm.api.*;
import ru.habibrahmanov.tm.repository.ProjectRepository;
import ru.habibrahmanov.tm.repository.TaskRepository;
import ru.habibrahmanov.tm.repository.UserRepository;
import ru.habibrahmanov.tm.service.ProjectService;
import ru.habibrahmanov.tm.service.TaskService;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.service.UserService;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public final class Bootstrap implements ServiceLocator {
    private IProjectRepository iProjectRepository = new ProjectRepository();
    private ITaskRepository iTaskRepository = new TaskRepository();
    private IUserRepository iUserRepository = new UserRepository();
    private IProjectService iProjectService = new ProjectService(iProjectRepository);
    private ITaskService iTaskService = new TaskService(iTaskRepository);
    private IUserService iUserService = new UserService(iUserRepository);
    private Scanner scanner = new Scanner(System.in);
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public void init(final Class... commandsClass) {
        try {
            if (commandsClass == null) return;
            for (Class clazz : commandsClass) {
                registry(clazz);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void registry(Class clazz) throws IllegalAccessException, InstantiationException {
        AbstractCommand abstractCommand = (AbstractCommand) clazz.newInstance();
        abstractCommand.setBootstrap(this);
        final String nameCommand = abstractCommand.getName();
        final String descriptionCommand = abstractCommand.getDescription();
        if (nameCommand == null || nameCommand.isEmpty()) return;
        if (descriptionCommand == null || descriptionCommand.isEmpty()) return;
        commands.put(nameCommand, abstractCommand);
    }

    @Override
    public void start() throws Exception {
        System.out.println("***WELCOME TO TASK MANAGER***");
        System.out.println("Enter \"help\" to show all commands.");
        String command = "";
        while (!command.equals("exit")) {
            command = scanner.nextLine();
            execute(command);
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        if (abstractCommand.secure() || (!abstractCommand.secure() && iUserService.isAuth())) {
            abstractCommand.execute();
        }
    }

    @Override
    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    @Override
    public IProjectService getProjectService() {
        return iProjectService;
    }

    @Override
    public ITaskService getTaskService() {
        return iTaskService;
    }

    @Override
    public Scanner getScanner() {
        return scanner;
    }

    @Override
    public IUserService getUserService() {
        return iUserService;
    }

    @Override
    public IUserRepository getUserRepository() {
        return iUserRepository;
    }
}
