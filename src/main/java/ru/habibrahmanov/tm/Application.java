package ru.habibrahmanov.tm;

import ru.habibrahmanov.tm.api.ServiceLocator;
import ru.habibrahmanov.tm.bootstrap.Bootstrap;
import ru.habibrahmanov.tm.command.util.AboutCommand;
import ru.habibrahmanov.tm.command.util.HelpCommand;
import ru.habibrahmanov.tm.command.project.*;
import ru.habibrahmanov.tm.command.task.*;
import ru.habibrahmanov.tm.command.user.*;

public class Application {
    public static final Class[] COMMANDS = {

            HelpCommand.class, AboutCommand.class,
            ProjectFindAllCommand.class, ProjectMergeCommand.class, ProjectPersistCommand.class, ProjectRemoveAllCommand.class,
            ProjectRemoveOneCommand.class, ProjectUpdateCommand.class,
            TaskFindAllCommand.class, TaskFindOneCommand.class, TaskMergeCommand.class, TaskPersistCommand.class,
            TaskRemoveOneCommand.class, TaskUpdateCommand.class,
            UserLoginCommand.class, UserEditProfileCommand.class, UserEditProfileCommand.class, UserRegistryCommand.class,
            UserUpdatePasswordCommand.class, UserViewProfileCommand.class, AdminRegistryCommand.class, UserLogoutCommand.class
    };

    public static void main(String[] args) throws Exception {
        ServiceLocator serviceLocator = new Bootstrap();
        serviceLocator.init(COMMANDS);
        serviceLocator.start();
    }
}
