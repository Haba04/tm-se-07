package ru.habibrahmanov.tm.command.project;

import ru.habibrahmanov.tm.command.AbstractCommand;

public final class ProjectPersistCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-persist";
    }

    @Override
    public String getDescription() {
        return "create new project.";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = serviceLocator.getScanner().nextLine();
        serviceLocator.getProjectService().persist(name, serviceLocator.getUserService().getCurrentUser().getId());
        System.out.println("CREATE NEW PROJECT SUCCESSFULLY");
    }
}
