package ru.habibrahmanov.tm.command.project;

import ru.habibrahmanov.tm.command.AbstractCommand;

public final class ProjectUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-update";
    }

    @Override
    public String getDescription() {
        return "update project by id";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT EDIT]");
        System.out.println("ENTER ID");
        final String projectId = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER NAME TO CHANGE");
        final String name = serviceLocator.getScanner().nextLine();
        serviceLocator.getProjectService().update(serviceLocator.getUserService().getCurrentUser().getId(), projectId, name);
        System.out.println("PROJECT EDIT SUCCESSFULLY");
    }
}
