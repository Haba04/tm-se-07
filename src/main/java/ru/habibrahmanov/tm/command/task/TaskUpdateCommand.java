package ru.habibrahmanov.tm.command.task;

import ru.habibrahmanov.tm.command.AbstractCommand;

public final class TaskUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-update";
    }

    @Override
    public String getDescription() {
        return "update task by id";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK UPDATE]");
        System.out.println("ENTER TASK ID:");
        final String taskId = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER TASK NAME:");
        final String name = serviceLocator.getScanner().nextLine();
        serviceLocator.getTaskService().update(serviceLocator.getUserService().getCurrentUser().getId(), taskId, name);
        System.out.println("TASK UPDATED SUCCESSFULLY");
    }
}
