package ru.habibrahmanov.tm.command.task;

import ru.habibrahmanov.tm.command.AbstractCommand;

public final class TaskMergeCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-merge";
    }

    @Override
    public String getDescription() {
        return "update if task is already exist, else create new task";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK MERGE]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER TASK NAME:");
        final String name = serviceLocator.getScanner().nextLine();
        final String userId = serviceLocator.getUserService().getCurrentUser().getId();
        serviceLocator.getTaskService().merge(projectId, taskId, name, userId);
        System.out.println("TASK MERGE SUCCESSFULLY");
    }
}
