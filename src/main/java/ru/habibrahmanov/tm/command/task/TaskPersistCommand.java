package ru.habibrahmanov.tm.command.task;

import ru.habibrahmanov.tm.command.AbstractCommand;

public final class TaskPersistCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-persist";
    }

    @Override
    public String getDescription() {
        return "create new task";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER PROJECT ID");
        final String projectId = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER TASK NAME");
        final String name = serviceLocator.getScanner().nextLine();
        final String userId = serviceLocator.getUserService().getCurrentUser().getId();
        serviceLocator.getTaskService().persist(projectId, userId, name);
        System.out.println("TASK CREATE SUCCESSFULLY");
    }
}
