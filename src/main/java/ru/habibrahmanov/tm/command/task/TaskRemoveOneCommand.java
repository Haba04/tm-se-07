package ru.habibrahmanov.tm.command.task;

import ru.habibrahmanov.tm.command.AbstractCommand;

public final class TaskRemoveOneCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "remove all tasks";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER TASK ID:");
        final String taskId = serviceLocator.getScanner().nextLine();
        serviceLocator.getTaskService().remove(taskId);
        System.out.println("TASK REMOVED SUCCESSFULLY");
    }
}
