package ru.habibrahmanov.tm.command.util;

import com.jcabi.manifests.Manifests;
import ru.habibrahmanov.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "project assembly information";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT ASSEMBLY INFORMATION]");
        System.out.println("project version: " + Manifests.read("Version"));
        System.out.println("developer: " + Manifests.read("Developer"));
        System.out.println("build number: " + Manifests.read("BuildNumber"));
    }
}
