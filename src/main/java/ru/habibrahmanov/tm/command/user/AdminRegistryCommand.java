package ru.habibrahmanov.tm.command.user;

import ru.habibrahmanov.tm.command.AbstractCommand;

public final class AdminRegistryCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "admin-registry";
    }

    @Override
    public String getDescription() {
        return "new user registration - admin";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CREATE NEW USER-ADMIN]");
        System.out.println("ENTER LOGIN:");
        final String login = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER PASSWORD AGAIN:");
        final String passwordConfirm = serviceLocator.getScanner().nextLine();
        serviceLocator.getUserService().registryAdmin(login, password, passwordConfirm);
        System.out.println("CREATE NEW USER-ADMIN SUCCESSFULLY");
    }
}
