package ru.habibrahmanov.tm.service;

import ru.habibrahmanov.tm.api.ITaskRepository;
import ru.habibrahmanov.tm.api.ITaskService;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.entity.Task;

import java.util.List;
import java.util.UUID;

public final class TaskService implements ITaskService {
    private final ITaskRepository iTaskRepository;

    public TaskService(ITaskRepository iTaskRepository) {
        this.iTaskRepository = iTaskRepository;
    }

    @Override
    public void persist(final String projectId, final String userId, final String name) throws IncorrectValueException {
        if (projectId == null || projectId.isEmpty()) {
            throw new IncorrectValueException();
        }
        iTaskRepository.persist(new Task(name, UUID.randomUUID().toString(), projectId, userId));
    }

    @Override
    public Task findOne(final String taskId, final String userId) throws IncorrectValueException {
        if (taskId == null || taskId.isEmpty()) {
            throw new IncorrectValueException();
        }
        return iTaskRepository.findOne(taskId, userId);
    }

    @Override
    public List<Task> findAll(final String userId) throws IncorrectValueException {
        if (iTaskRepository.findAll(userId).isEmpty()) {
            throw new IncorrectValueException("LIST IS EMPTY");
        }
        return iTaskRepository.findAll(userId);
    }

    @Override
    public void remove(final String taskId) throws IncorrectValueException {
        if (taskId == null || taskId.isEmpty()) {
            throw new IncorrectValueException();
        }
        iTaskRepository.remove(taskId);
    }

    @Override
    public void removeAll(final String projectId) {
        iTaskRepository.removeAll(projectId);
    }


    @Override
    public void update(String userId, String taskId, String name) throws IncorrectValueException {
        if (userId == null || userId.isEmpty() || taskId == null || taskId.isEmpty() || name == null || name.isEmpty()) {
            throw new IncorrectValueException();
        }
        iTaskRepository.update(userId, taskId, name);
    }

    @Override
    public void merge(final String projectId, final String taskId, final String name, final String userId) throws IncorrectValueException {
        if (taskId == null || taskId.isEmpty() || name == null || name.isEmpty()) {
            throw new IncorrectValueException();
        }
        iTaskRepository.merge(new Task(name, taskId, projectId, userId));
    }
}
