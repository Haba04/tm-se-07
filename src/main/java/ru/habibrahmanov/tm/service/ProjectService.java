package ru.habibrahmanov.tm.service;

import ru.habibrahmanov.tm.api.IProjectRepository;
import ru.habibrahmanov.tm.api.IProjectService;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.entity.Project;

import java.util.List;
import java.util.UUID;

public final class ProjectService implements IProjectService {
    private final IProjectRepository iProjectRepository;

    public ProjectService(IProjectRepository iProjectRepository) {
        this.iProjectRepository = iProjectRepository;
    }

    @Override
    public void persist(final String projectName, final String userId) throws IncorrectValueException {
        if (projectName == null || projectName.isEmpty()){
            throw new IncorrectValueException();
        }
        iProjectRepository.persist(new Project(projectName, UUID.randomUUID().toString(), userId));
    }

    @Override
    public List<Project> findAll(final String userId) throws IncorrectValueException {
        if (iProjectRepository.findAll(userId).isEmpty()) {
            throw new IncorrectValueException("PROJECT LIST EMPTY");
        }
        return iProjectRepository.findAll(userId);
    }

    @Override
    public void removeAll() throws IncorrectValueException {
        if (iProjectRepository.getProjectMap().isEmpty()) {
            throw new IncorrectValueException("PROJECT LIST EMPTY");
        }
        iProjectRepository.removeAll();
    }

    @Override
    public void remove(final String projectId) throws IncorrectValueException {
        if (projectId == null || projectId.isEmpty()) {
            throw new IncorrectValueException();
        }
        iProjectRepository.remove(projectId);
    }

    @Override
    public void update(String userId, String projectId, String name) throws IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        iProjectRepository.update(userId, projectId, name);
    }

    @Override
    public void merge(final String projectId, final String name, final String userId) throws IncorrectValueException {
        if (projectId == null || projectId.isEmpty() || name == null || name.isEmpty()) {
            throw new IncorrectValueException();
        }
        iProjectRepository.merge(new Project(name, projectId, userId));
    }

    public IProjectRepository getiProjectRepository() {
        return iProjectRepository;
    }
}
