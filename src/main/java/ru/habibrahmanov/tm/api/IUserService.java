package ru.habibrahmanov.tm.api;

import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * @author Egor Laptiev
 */
public interface IUserService {
    void registryAdmin(String login, String password, String passwordConfirm) throws IllegalArgumentException, UnsupportedEncodingException, NoSuchAlgorithmException;

    void registryUser(String login, String password, String passwordConfirm) throws UnsupportedEncodingException, NoSuchAlgorithmException;

    void updatePassword(User currentUser, String curPassword, String newPassword, String newPasswordConfirm) throws UnsupportedEncodingException, NoSuchAlgorithmException;

    User login(String login, String password) throws IncorrectValueException, UnsupportedEncodingException, NoSuchAlgorithmException;

    User viewProfile();

    void editProfile(String newLogin) throws IncorrectValueException;

    void logout();

    boolean isAuth();

    String createPasswordHashMD5(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException;

    User getCurrentUser();
}
