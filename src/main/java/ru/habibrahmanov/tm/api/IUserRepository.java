package ru.habibrahmanov.tm.api;

import ru.habibrahmanov.tm.entity.User;

import java.util.List;

/**
 * @author Egor Laptiev
 */
public interface IUserRepository {
    void persist(User user);

    User findOne(String userId);

    User findByLogin(String login);

    List<User> findAll();

    void removeOne(String login);

    void removeAll();

    void update(String userId, String login);
}
