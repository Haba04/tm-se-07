package ru.habibrahmanov.tm.api;

import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;

import java.util.List;

public interface IProjectService {
    void persist(String projectName, String userId) throws IncorrectValueException;

    List<Project> findAll(String userId) throws IncorrectValueException;

    void removeAll() throws IncorrectValueException;

    void remove(String projectId) throws IncorrectValueException;

    void update(String userId, String projectId, String name) throws IncorrectValueException;

    void merge(String projectId, String name, String userId) throws IncorrectValueException;
}
