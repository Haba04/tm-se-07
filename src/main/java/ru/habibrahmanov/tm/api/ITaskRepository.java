package ru.habibrahmanov.tm.api;

import ru.habibrahmanov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {
    void persist(Task task);

    Task findOne(String taskId, String userId);

    List<Task> findAll(String userId);

    void remove(String taskId);

    boolean removeAll(String projectId);

    void update(String userId, String taskId, String name);

    void merge(Task task);
}
