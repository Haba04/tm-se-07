package ru.habibrahmanov.tm.api;

import ru.habibrahmanov.tm.entity.Project;

import java.util.List;
import java.util.Map;

public interface IProjectRepository {
    void persist(Project project);

    List<Project> findAll(String userId);

    Project findOne(String projectId);

    void removeAll();

    void remove(String projectId);

    void merge(Project project);

    void update(String userId, String projectId, String name);

    public Map<String, Project> getProjectMap();
}
