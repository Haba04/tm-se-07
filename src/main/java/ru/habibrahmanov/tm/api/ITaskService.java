package ru.habibrahmanov.tm.api;

import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;

import java.util.List;

public interface ITaskService {
    void persist(String projectId, String userId, String name) throws IncorrectValueException;

    Task findOne(String taskId, String userId) throws IncorrectValueException;

    List<Task> findAll(String userId) throws IncorrectValueException;

    void remove(String taskId) throws IncorrectValueException;

    void removeAll(String projectId);

    void update(String userId, String taskId, String name) throws IncorrectValueException;

    void merge(String projectId, String taskId, String name, String userId) throws IncorrectValueException;
}
