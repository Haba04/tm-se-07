package ru.habibrahmanov.tm.api;

import ru.habibrahmanov.tm.command.AbstractCommand;

import java.util.Map;
import java.util.Scanner;

public interface ServiceLocator {
    void init(Class... commandsClass);

    void registry(Class clazz) throws IllegalAccessException, InstantiationException;

    void start() throws Exception;

    Map<String, AbstractCommand> getCommands();

    IProjectService getProjectService();

    ITaskService getTaskService();

    Scanner getScanner();

    IUserService getUserService();

    IUserRepository getUserRepository();
}
